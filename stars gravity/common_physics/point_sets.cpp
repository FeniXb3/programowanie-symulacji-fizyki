/** @file point_sets.cpp
 *  @brief implementation of different point sets
 *
 *	@author Bartlomiej Filipek
 *	@date April 2011
 */

#include "stdafx.h"
#include "material_point.h"
#include "point_sets.h"

///////////////////////////////////////////////////////////////////////////////
// StarsPointSet
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
StarsPointSet::StarsPointSet(unsigned int count) : MaterialPointSet(count)
{

}

///////////////////////////////////////////////////////////////////////////////
Vec3d StarsPointSet::Force(unsigned int i)
{
	Vec3d force(0.0);

	if(!m_points[i].movable)
		return force;

	double G = 0.00000000006673848;

	for(int j = 0; j < m_count; j++)
	{
		if(j != i)
		{
			double rx = m_points[i].m_pos.x - m_points[j].m_pos.x;
			double ry = m_points[i].m_pos.y - m_points[j].m_pos.y;
			double rz = m_points[i].m_pos.z - m_points[j].m_pos.z;
			
			Vec3d deltapos = m_points[i].m_pos - m_points[j].m_pos;

			Vec3d tmpForce(0);
			double len = deltapos.Length();

			tmpForce = G*(m_points[i].m_mass*m_points[j].m_mass)/(len*len);
			
			force.x -= tmpForce.x * (deltapos.x/len);
			force.y -= tmpForce.y * (deltapos.y/len);
		}
	}
	
	return force;
}

