/** @file point_sets.cpp
 *  @brief implementation of different point sets
 *
 *	@author Bartlomiej Filipek
 *	@date April 2011
 */

#include "stdafx.h"
#include "material_point.h"
#include "point_sets.h"

///////////////////////////////////////////////////////////////////////////////
// OscilatorPointSet
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
OscilatorPointSet::OscilatorPointSet(unsigned int count) : MaterialPointSet(count)
{
	m_baseLen = 0.0;
	m_k = 0.0;
	m_resistance = 0.0;
	m_suppression = 0.0;
	m_forceOnStretch = false;
}

///////////////////////////////////////////////////////////////////////////////
Vec3d OscilatorPointSet::Force(unsigned int i)
{
	/* pseudokod: ************************
	
	sila = 0; sila_prawy = 0; sila_lewy = 0;

	// policz sile dla lewy:
	vec r = points[i-1].pos - points[i]
	zapisz dlugosc "r" w zmieniej nowej "len"
	gdy "len" wieksza od dlugosci spoczynkowej
		znormalizuj "r"
		sila_lewy = k*("len" - dlugosc_spoczynkowa) * r
		// tu wstaw sily tlumienia

	// analogicznie dla prawy
	...

	// tu dodaj sile oporu

	sila = sila_prawy + sila_lewy
	return sila;

	*************************************/ 
	
	Vec3d force(0.0), forceLeft(0.0), forceRight(0.0), forceResistance(0.0), forceSuppression(0.0), r(0.0);
	
	if(i>0)
	{
		r = m_points[i-1].m_pos - m_points[i].m_pos;
	
		double len = r.Length();
		if(len > m_baseLen || m_forceOnStretch)
		{
			r.Normalize();
			forceLeft = m_k*(len - m_baseLen)*r;


			Vec3d velDelta = m_points[i-1].m_vel - m_points[i].m_vel;
			forceSuppression = -2*m_suppression*DotProduct(velDelta,r)*r;
			forceLeft = forceLeft - forceSuppression;
		}
	}

	if(i < m_count-1)
	{
		r = m_points[i+1].m_pos - m_points[i].m_pos;
		double len = r.Length();
		if(len > m_baseLen || m_forceOnStretch)
		{
			r.Normalize();
			forceRight = m_k*(len - m_baseLen)*r;

			Vec3d velDelta = m_points[i+1].m_vel - m_points[i].m_vel;
			forceSuppression = -2*m_suppression*DotProduct(velDelta,r)*r;
			forceRight = forceRight - forceSuppression;
		}
	}

	forceResistance = -2*m_resistance*m_points[i].m_vel;
	

	force = forceRight + forceLeft + forceResistance;
	return force;
}

///////////////////////////////////////////////////////////////////////////////
// StiffOscilatorPointSet
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
StiffOscilatorPointSet::StiffOscilatorPointSet(unsigned int count) :
	OscilatorPointSet(count)
{
	m_stiffnessForces = new Vec3d[count];
	m_forceOnStretch = true;
	m_stiffness = 0.1;
}

///////////////////////////////////////////////////////////////////////////////
Vec3d StiffOscilatorPointSet::Force(unsigned int i)
{
	Vec3d force = OscilatorPointSet::Force(i);
	if (m_stiffness > 0.0) force += m_stiffnessForces[i];

	return force;
}

///////////////////////////////////////////////////////////////////////////////
void StiffOscilatorPointSet::BeforeStep(double deltaTime)
{
	/* pseudokod: ************************
	
	function BeforeStep()
	{
		for each point: sila_sztywnosci[point] = 0;

		for each point:
			do_lewego = point[i-1].pos - point[i].pos
			do_prawego = point[i+1].pos - point[i].pos
			sila = s*(do_prawego + do_lewego)/2

			sila_sztywnosci[i] += sila;
			sila_sztywnosci[i-1] -= sila/2;
			sila_sztywnosci[i+1] -= sila/2;
	}

	function Sila(i)
	{
		return sila_wlasciwa(i) + sila_sztywnosci[i]
	}

	*************************************/ 
	
	///////////////////
	// TODO
	///////////////////
	for(int i = 0; i < m_count; i++)
	{
		m_stiffnessForces[i] = Vec3d(0.0);
	}

	
	for(int i = 1; i < m_count - 1; i++)
	{
		Vec3d stiffLeft(0.0), stiffRight(0.0), force(0.0);
		
		stiffLeft = m_points[i-1].m_pos - m_points[i].m_pos;
		stiffRight = m_points[i+1].m_pos - m_points[i].m_pos;

		force = m_stiffness*(stiffLeft+stiffRight)/2;

		m_stiffnessForces[i] += force;
		m_stiffnessForces[i-1] += force/2;
		m_stiffnessForces[i+2] += force/2;
		
	}
}

///////////////////////////////////////////////////////////////////////////////
// LinePointSet
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
LinePointSet::LinePointSet(unsigned int count) :
	StiffOscilatorPointSet(count),
	m_gForce(0.0, -9.0, 0.0)
{
	m_forceOnStretch = true;
}


