#include "StdAfx.h"
#include "MaterialPoint.h"


MaterialPoint::MaterialPoint(void)
{
	m_vel = Vec3d(0,1,0);
	m_algType = atEuler;
	m_radius = 0.5;
	m_pos = Vec3d(0,0,0);
	m_mass = 10;
}


MaterialPoint::~MaterialPoint(void)
{
}

void MaterialPoint::PrepareMove(double deltaTime, const Vec3d &force)
{
	m_acceleration = force * (1.0/m_mass);
	if (m_algType == atEuler || m_stepCounter++ == 0) CalcEuler(deltaTime);
	else CalcVerlet(deltaTime);
}


void MaterialPoint::CalcEuler(double delta)
{
	m_nextVel = m_vel + m_acceleration*delta;
	m_nextPos = m_pos + m_nextVel*delta;

}

void MaterialPoint::CalcVerlet(double delta)
{
	m_nextPos = 2*m_pos - m_prevPos + m_acceleration*(delta*delta);
	m_nextVel = (m_nextPos - m_prevPos)/(2*delta);
}

void MaterialPoint::UpdateMove()
{
	m_vel = m_nextVel;
	m_prevPos = m_pos;
	m_pos = m_nextPos;
}

void MaterialPoint::Reset()
{
	m_vel = Vec3d(0,1,0);
	m_pos = Vec3d(0,0,0);
	m_stepCounter = 0;
}