/** @file utils.h
 *  @brief useful utilities for opengl based apps
 *
 *	@author Bartlomiej Filipek
 *	@date March 2011
 */

#pragma once

/** logging will be by using simple "printf" function */
#define UT_LOG_WITH_PRINTF

#ifdef UT_LOG_WITH_PRINTF
	#define utLOG(msg, ...) { printf("%s: ", __FUNCTION__); printf(msg, __VA_ARGS__); printf("\n"); }
	#define utLOG_ERROR(msg, ...) { printf("ERR in %s, %d: ", __FUNCTION__, __LINE__); printf(msg, __VA_ARGS__); printf("\n"); }
#else
// TODO: implement
#endif

///////////////////////////////////////////////////////////////////////////////
// 2D and TEXT MODE
void utSetOrthographicProjection(int scrW, int scrH);
void utResetPerspectiveProjection();
void utDrawText2D(float x, float y, void *font, char *string);

// simplier version for default font...
inline void utDrawText2D(float x, float y, char *string) {
	utDrawText2D(x, y, (void *)GLUT_BITMAP_8_BY_13, string);
}

template <typename T> 
class TVector3 
{ 
public: 
      // dzi�ki unii b�dziemy mogli si� dostawa� do Pol klasy zar�wno po 
      // nazwie wsp�rz�dnej, jak i poprzez adres w tablicy... 
      union 
      { 
            T m[3]; 
            struct 
            { 
                  T x, y, z; 
            }; 
      }; 
public: 
      TVector3() { } 
      TVector3(const T& a, const T& b, const T& c): x(a), y(b), z(c) { } 
 
// przyk�adowa implementacja dodawania 
friend TVector3<T> operator+ (const TVector3<T> & a, const TVector3<T> & b) 
{ return TVector3<T>(a.x+b.x, a.y+b.y, a.z+b.z); } 
 
// niezaimplementowane operatory � do zrobienia! 
friend TVector3<T> operator- (const TVector3<T> & a, const TVector3<T> & b) 
{return TVector3<T>(a.x-b.x, a.y-b.y, a.z-b.z); } 
 
friend TVector3<T> operator/ (const TVector3<T> & a, const T &v) { return TVector3<T>(a.x/v, a.y/v, a.z/v); }

friend TVector3<T> operator* (const TVector3<T> & a, const T &v) { return TVector3<T>(a.x*v, a.y*v, a.z*v);} 
 
friend TVector3<T> operator* (const T &v, const TVector3<T> & a) {  return TVector3<T>(a.x*v, a.y*v, a.z*v);} 
}; 
 
// specyfikacja szablonu dla typu float oraz double: 
typedef TVector3<float> Vec3f; 
typedef TVector3<double> Vec3d; 